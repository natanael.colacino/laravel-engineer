<?php

namespace App\Providers;

use App\Infra\LaravelDispatcher;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Infra\EmailSender\LaravelEmailSender;
use App\Infra\Repositories\EmployeeSqliteRepository;
use App\Domain\Services\Dispatcher\DispatcherInterface;
use App\Domain\Repositories\EmployeeRepositoryInterface;
use App\Domain\Services\EmailSender\EmailSenderInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Gate::define('employee', 'App\Policies\EmployeePolicy@authorize');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EmailSenderInterface::class, LaravelEmailSender::class);
        $this->app->bind(DispatcherInterface::class, LaravelDispatcher::class);

        $this->app->bind(EmployeeRepositoryInterface::class, EmployeeSqliteRepository::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Domain\Entities\User;
use Illuminate\Http\JsonResponse;
use App\Domain\ValueObjects\Email;
use App\Http\Requests\ImportRequest;
use App\Domain\UseCases\ImportEmployeeUseCase;
use App\Domain\Exceptions\InvalidFormatException;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function import(ImportRequest $request, ImportEmployeeUseCase $importEmployeeUseCase)
    {
        try {
            $importerUser = new User(
                $request->user()->id,
                $request->user()->name,
                new Email($request->user()->email)
            );

            $importEmployeeUseCase->execute(
                $request->file('employees')->getContent(),
                $request->file('employees')->getClientOriginalName(),
                $importerUser
            );
        } catch (InvalidFormatException $exception) {
            $json = ['message' => $exception->getMessage()];

            return response()->json($json, 406);
        }
    }
}

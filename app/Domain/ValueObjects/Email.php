<?php

namespace App\Domain\ValueObjects;

use App\Domain\Exceptions\InvalidFormatException;

class Email
{
    private $value;

    public function __construct(string $email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
            throw new InvalidFormatException('The email attribute must contain a valid email address.');
        }

        $this->value = $email;
    }

    public function __toString()
    {
        return $this->value;
    }
}

<?php

namespace App\Domain\Services\Dispatcher;

interface DispatcherInterface
{
    public function dispatch(Job $job);
}

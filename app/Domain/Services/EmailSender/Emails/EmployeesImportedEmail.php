<?php

namespace App\Domain\Services\EmailSender\Emails;

class EmployeesImportedEmail extends Email
{
    public function __construct(
        public string $filename
    ) {
    }
}

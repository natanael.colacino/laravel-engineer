<?php

namespace App\Domain\Services\EmailSender;

use App\Domain\Entities\User;
use App\Domain\Services\EmailSender\Emails\Email;

interface EmailSenderInterface
{
    public function send(Email $email, User $user): void;
}

<?php

namespace App\Domain\Repositories;

use App\Domain\Entities\User;
use App\Domain\Entities\Employee;

interface EmployeeRepositoryInterface
{
    /**
     * @param array<Employee> $employees
     */
    public function import(array $employees, User $importerUser);
}

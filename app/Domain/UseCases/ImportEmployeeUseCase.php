<?php

namespace App\Domain\UseCases;

use Illuminate\Support\Str;
use App\Domain\Entities\User;
use App\Domain\Entities\Employee;
use App\Domain\ValueObjects\Email;
use App\Domain\Jobs\ImportEmployeesJob;
use App\Domain\Services\Dispatcher\DispatcherInterface;

class ImportEmployeeUseCase
{
    public function __construct(
        private DispatcherInterface $dispatcher
    ) {
    }

    public function execute(string $fileContent, string $filename, User $importerUser): void
    {
        $employees = $this->convertFileContentToArrayOfEmployees($fileContent);

        $this->dispatcher->dispatch(
            new ImportEmployeesJob($employees, $filename, $importerUser)
        );
    }

    /**
     * @return array<Employee>
     */
    private function convertFileContentToArrayOfEmployees(string $fileContent): array
    {
        $collectionOfLines = Str::of($fileContent)->explode("\n");
        $fileData = $collectionOfLines->map(fn ($line) => Str::of($line)->explode(','));

        $headers = $fileData->shift();

        $fileData = $fileData
            ->filter(fn ($lineExploded) => $lineExploded->count() == $headers->count())
            ->map(fn ($lineExploded) => $headers->combine($lineExploded));

        return $fileData->map(function ($line) {
            return new Employee(
                name: $line['name'],
                email: new Email($line['email']),
                document: $line['document'],
                city: $line['city'],
                state: $line['state'],
                startDate: $line['start_date'],
            );
        })->toArray();
    }
}

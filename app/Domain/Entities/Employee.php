<?php

namespace App\Domain\Entities;

use App\Domain\ValueObjects\Email;
use App\Domain\Exceptions\InvalidFormatException;

class Employee
{
    public function __construct(
        public string $name,
        public Email $email,
        public string $document,
        public string $city,
        public string $state,
        public string $startDate,
    ) {
        if (strlen($this->state) > 2) {
            throw new InvalidFormatException('The state attribute must contain only 2 characters.');
        }

        $exploded = explode('-', $this->startDate);

        if (count($exploded) != 3 || !checkdate((int)$exploded[1], (int)$exploded[2], (int)$exploded[0])) {
            throw new InvalidFormatException('Invalid Date in attribute field.');
        }
    }
}

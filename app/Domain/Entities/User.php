<?php

namespace App\Domain\Entities;

use App\Domain\ValueObjects\Email;

class User
{
    public function __construct(
        public int $id,
        public string $name,
        public Email $email,
    ) {
    }
}

<?php

namespace App\Domain\Jobs;

use App\Domain\Entities\User;
use App\Domain\Services\Dispatcher\Job;
use App\Domain\Repositories\EmployeeRepositoryInterface;
use App\Domain\Services\EmailSender\EmailSenderInterface;
use App\Domain\Services\EmailSender\Emails\EmployeesImportedEmail;

class ImportEmployeesJob extends Job
{
    public function __construct(
        private array $employees,
        private string $filename,
        private User $importerUser,
    ) {
    }

    public function handle(EmployeeRepositoryInterface $employeeRepository, EmailSenderInterface $emailSender)
    {
        $employeeRepository->import($this->employees, $this->importerUser);

        $emailSender->send(
            new EmployeesImportedEmail($this->filename),
            $this->importerUser
        );
    }
}

<?php

namespace App\Domain\Exceptions;

use Exception;

class InvalidFormatException extends Exception
{
}

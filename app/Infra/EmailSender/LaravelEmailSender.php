<?php

namespace App\Infra\EmailSender;

use App\Domain\Entities\User;
use Illuminate\Support\Facades\Mail;
use App\Domain\Services\EmailSender\Emails\Email;
use App\Infra\EmailSender\Emails\EmployeesImported;
use App\Domain\Services\EmailSender\EmailSenderInterface;
use App\Domain\Services\EmailSender\Emails\EmployeesImportedEmail;

class LaravelEmailSender implements EmailSenderInterface
{
    public function send(Email $email, User $user): void
    {
        if ($email instanceof EmployeesImportedEmail) {
            $laravelEmail = new EmployeesImported($email->filename);
        }

        $userForLaravel = (object) [
            'name' => $user->name,
            'email' => (string) $user->email,
        ];

        Mail::to($userForLaravel)->send($laravelEmail);
    }
}

<?php

namespace App\Infra\Repositories;

use App\Domain\Entities\User;
use App\Models\Employee as EmployeeModel;
use App\Domain\Repositories\EmployeeRepositoryInterface;

class EmployeeSqliteRepository implements EmployeeRepositoryInterface
{
    public function import(array $employees, User $importerUser)
    {
        $dataToInsert = [];

        foreach ($employees as $employee) {
            $dataToInsert[] = [
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->startDate,
                'user_id' => $importerUser->id,
            ];
        }

        $uniquelyIdentify = ['document'];
        $columnsToUpdate = ['name', 'email', 'city', 'state', 'start_date', 'user_id'];

        return EmployeeModel::upsert($dataToInsert, $uniquelyIdentify, $columnsToUpdate);
    }
}

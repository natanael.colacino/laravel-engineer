<?php

namespace App\Infra;

use App\Domain\Services\Dispatcher\Job;
use App\Domain\Services\Dispatcher\DispatcherInterface;

class LaravelDispatcher implements DispatcherInterface
{
    public function dispatch(Job $job)
    {
        dispatch($job);
    }
}

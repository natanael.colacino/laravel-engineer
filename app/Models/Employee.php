<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'document',
        'city',
        'state',
        'start_date',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}

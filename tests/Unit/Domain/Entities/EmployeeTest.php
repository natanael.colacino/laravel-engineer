<?php

namespace Tests\Unit\Domain\Entities;

use PHPUnit\Framework\TestCase;
use App\Domain\Entities\Employee;
use App\Domain\ValueObjects\Email;
use App\Domain\Exceptions\InvalidFormatException;

class EmployeeTest extends TestCase
{
    public function test_it_can_instantiate_a_valid_employee()
    {
        $employee = new Employee(
            name: 'João',
            email: new Email('joao@teste.com.br'),
            document: '431.986.320-80',
            city: 'Ribeirão Preto',
            state:'SP',
            startDate:'2023-05-20',
        );

        $this->assertInstanceOf(Employee::class, $employee);
    }

    public function test_it_fails_passing_a_invalid_date()
    {
        $this->expectException(InvalidFormatException::class);

        new Employee(
            name: 'João',
            email: new Email('joao@teste.com.br'),
            document: '431.986.320-80',
            city: 'Ribeirão Preto',
            state: 'SP',
            startDate: '2021-02-29',
        );
    }

    public function test_it_fails_passing_a_invalid_state_format()
    {
        $this->expectException(InvalidFormatException::class);

        new Employee(
            name: 'João',
            email: new Email('joao@teste.com.br'),
            document: '431.986.320-80',
            city: 'Ribeirão Preto',
            state: 'São Paulo',
            startDate: '2023-05-20',
        );
    }
}

<?php

namespace Tests\Unit\Domain\Jobs;

use App\Domain\Entities\User;
use PHPUnit\Framework\TestCase;
use App\Domain\Entities\Employee;
use App\Domain\ValueObjects\Email;
use App\Domain\Jobs\ImportEmployeesJob;
use App\Domain\Repositories\EmployeeRepositoryInterface;
use App\Domain\Services\EmailSender\EmailSenderInterface;
use App\Domain\Services\EmailSender\Emails\EmployeesImportedEmail;

class ImportEmployeesJobTest extends TestCase
{
    private ImportEmployeesJob $importEmployeesJob;

    private EmailSenderInterface $emailSender;
    private EmployeeRepositoryInterface $employeeRepository;

    private string $filename;
    private array $employees;
    private User $importerUser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->emailSender = $this->createMock(EmailSenderInterface::class);
        $this->employeeRepository = $this->createMock(EmployeeRepositoryInterface::class);

        $this->employees = [
            new Employee(
                name: 'BobWilson',
                email: new Email('bob@paopaocafe.com'),
                document: '13001647000',
                city: 'Salvador',
                state: 'BA',
                startDate: '2020-01-15',
            ),
            new Employee(
                name: 'LauraMatsuda',
                email: new Email('lm@matsuda.com.br'),
                document: '60095284028',
                city: 'Niterói',
                state: 'RJ',
                startDate: '2019-06-08',
            ),
        ];

        $this->filename = 'example.csv';

        $this->importerUser = new User(1, 'João', new Email('joao@teste.com.br'));

        $this->importEmployeesJob = new ImportEmployeesJob(
            $this->employees,
            $this->filename,
            $this->importerUser
        );
    }

    public function test_it_send_the_employees_imported_email_to_user()
    {
        $this->emailSender
            ->expects($this->once())
            ->method('send')
            ->with(new EmployeesImportedEmail($this->filename));

        $this->importEmployeesJob->handle($this->employeeRepository, $this->emailSender);
    }

    public function test_it_call_employee_repository_correctly()
    {
        $this->employeeRepository
            ->expects($this->once())
            ->method('import')
            ->with($this->employees, $this->importerUser);

        $this->importEmployeesJob->handle($this->employeeRepository, $this->emailSender);
    }
}

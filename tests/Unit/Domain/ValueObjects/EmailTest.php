<?php

namespace Tests\Unit\Domain\ValueObjects;

use PHPUnit\Framework\TestCase;
use App\Domain\ValueObjects\Email;
use App\Domain\Exceptions\InvalidFormatException;

class EmailTest extends TestCase
{
    public function test_it_can_instantiate_a_valid_email()
    {
        $email = new Email('fulado-de-tal@gmail.com');

        $this->assertInstanceOf(Email::class, $email);
    }

    public function test_it_fails_passing_a_invalid_email()
    {
        $this->expectException(InvalidFormatException::class);

        new Email('invalid-email');
    }
}

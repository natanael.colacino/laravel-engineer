<?php

namespace Tests\Unit\Domain\UseCases;

use App\Domain\Entities\User;
use PHPUnit\Framework\TestCase;
use App\Domain\Entities\Employee;
use App\Domain\ValueObjects\Email;
use App\Domain\Jobs\ImportEmployeesJob;
use App\Domain\UseCases\ImportEmployeeUseCase;
use App\Domain\Services\Dispatcher\DispatcherInterface;

class ImportEmployeeUseCaseTest extends TestCase
{
    private DispatcherInterface $dispatcher;
    private ImportEmployeeUseCase $importEmployeeUseCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dispatcher = $this->createMock(DispatcherInterface::class);
        $this->importEmployeeUseCase = new ImportEmployeeUseCase($this->dispatcher);
    }

    public function test_it_dispatch_import_employees_job_with_data_converted_correctly()
    {
        $expectedArrayOfEmployees = [
            new Employee(
                name: 'BobWilson',
                email: new Email('bob@paopaocafe.com'),
                document: '13001647000',
                city: 'Salvador',
                state: 'BA',
                startDate: '2020-01-15',
            ),
            new Employee(
                name: 'LauraMatsuda',
                email: new Email('lm@matsuda.com.br'),
                document: '60095284028',
                city: 'Niterói',
                state: 'RJ',
                startDate: '2019-06-08',
            ),
        ];

        $filename = 'example.csv';

        $user = new User(1, 'João', new Email('joao@teste.com.br'));

        $this->dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with(new ImportEmployeesJob($expectedArrayOfEmployees, $filename, $user));

        $fileContent = file_get_contents(__DIR__ . '/../../../Data/csv-with-two-valid-employees.csv');

        $this->importEmployeeUseCase->execute($fileContent, $filename, $user);
    }
}

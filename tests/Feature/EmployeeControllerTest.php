<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use Laravel\Passport\Passport;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function test_it_gets_all_employees_with_a_auth_user()
    {
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertJson(
                $employees->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'user_id' => $employee->user_id,
                        'name' => $employee->name,
                        'email' => $employee->email,
                        'document' => $employee->document,
                        'city' => $employee->city,
                        'state' => $employee->state,
                        'start_date' => $employee->start_date,
                    ];
                })->toArray()
            );
    }

    public function test_it_cannot_gets_employees_from_another_user()
    {
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function test_it_shows_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertOk()
            ->assertJson([
                'id' => $employee->id,
                'user_id' => $employee->user_id,
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->start_date,
            ]);
    }

    public function test_it_cannot_shows_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function test_it_deletes_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function test_it_cannot_deletes_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }

    public function test_it_can_upload_employees()
    {
        $file = $this->createFakeFile('csv-with-four-valid-employees.csv');

        $this->postJson(route('employees.import'), ['employees' => $file])->assertOk();

        $this->assertDatabaseCount('employees', 4);
        $this->assertEquals(4, Employee::where('user_id', $this->user->id)->count());
    }

    public function test_it_can_update_a_employee_with_the_employees_import()
    {
        Employee::factory()->create([
            'name' => 'Marco Rodrigues',
            'email' => 'marco@kyokugen.org',
            'document' => '71306511054',
            'city' => 'Osasco',
            'state' => 'SC',
            'start_date' => '2021-01-10',
        ]);

        $this->assertDatabaseCount('employees', 1);

        $file = $this->createFakeFile('csv-with-just-one-employee.csv');

        $this->postJson(route('employees.import'), ['employees' => $file])->assertOk();

        $this->assertDatabaseCount('employees', 1);

        $this->assertDatabaseHas('employees', [
            'user_id' => $this->user->id,
            'state' => 'SP',
        ]);
    }

    public function test_it_fails_when_pass_a_invalid_date_and_dont_change_database()
    {
        $employee = Employee::factory()->create();

        $this->assertDatabaseCount('employees', 1);

        $file = $this->createFakeFile('csv-with-a-invalid-date.csv');

        $this->postJson(route('employees.import'), ['employees' => $file])
            ->assertStatus(406)
            ->assertSee('Invalid Date');

        $this->assertDatabaseCount('employees', 1);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $employee->user_id,
            'name' => $employee->name,
            'email' => $employee->email,
            'document' => $employee->document,
            'city' => $employee->city,
            'state' => $employee->state,
            'start_date' => $employee->start_date,
        ]);
    }

    private function createFakeFile(string $filename): File
    {
        $content = file_get_contents(__DIR__ . '/../Data/' . $filename);

        return UploadedFile::fake()->createWithContent('employees.csv', $content);
    }
}

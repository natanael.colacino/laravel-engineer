<?php

namespace Tests\Feature\Infra\EmailSender\Emails;

use Tests\TestCase;
use App\Infra\EmailSender\Emails\EmployeesImported;

class EmployeesImportedTest extends TestCase
{
    public function test_if_email_content_has_the_filename()
    {
        $filename = 'example-file.csv';

        $mailable = new EmployeesImported($filename);

        $mailable->assertSeeInHtml($filename);
        $mailable->assertSeeInText($filename);
    }
}

<?php

namespace Tests\Feature\Infra\EmailSender;

use Tests\TestCase;
use App\Domain\Entities\User;
use App\Domain\ValueObjects\Email;
use Illuminate\Support\Facades\Mail;
use App\Infra\EmailSender\LaravelEmailSender;
use App\Infra\EmailSender\Emails\EmployeesImported;
use App\Domain\Services\EmailSender\Emails\EmployeesImportedEmail;

class LaravelEmailSenderTest extends TestCase
{
    public function test_it_send_email_to_user_correctly_when_give_employees_imported_email()
    {
        Mail::fake();

        $laravelEmailSender = new LaravelEmailSender();

        $email = new EmployeesImportedEmail('filename.csv');
        $user = new User(1, 'João', new Email('joao@teste.com.br'));

        $laravelEmailSender->send($email, $user);

        Mail::assertSent(EmployeesImported::class, function (EmployeesImported $mail) use ($user) {
            return $mail->hasTo((string) $user->email);
        });
    }
}

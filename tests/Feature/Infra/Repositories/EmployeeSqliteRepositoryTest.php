<?php

namespace Tests\Feature\Infra\Repositories;

use Tests\TestCase;
use App\Domain\Entities\User;
use App\Domain\Entities\Employee;
use App\Domain\ValueObjects\Email;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Infra\Repositories\EmployeeSqliteRepository;

class EmployeeSqliteRepositoryTest extends TestCase
{
    use RefreshDatabase;

    private User $importerUser;
    private EmployeeSqliteRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = new EmployeeSqliteRepository();
        $this->importerUser = new User(1, 'João', new Email('joao@teste.com.br'));
    }

    public function test_if_import_method_can_insert_the_correct_employees_on_database()
    {
        $john = new Employee(
            name: 'João',
            email: new Email('joao@teste.com.br'),
            document: '431.986.320-80',
            city: 'Ribeirão Preto',
            state: 'SP',
            startDate: '2023-05-20',
        );

        $mary = new Employee(
            name: 'Mary',
            email: new Email('mary@teste.com.br'),
            document: '496.115.710-46',
            city: 'Ribeirão Preto',
            state: 'SP',
            startDate: '2023-05-20',
        );

        $this->assertDatabaseCount('employees', 0);

        $this->repository->import([$john, $mary], $this->importerUser);

        $this->assertDatabaseCount('employees', 2);

        $this->assertDatabaseHas('employees', [
            'name' => $john->name,
            'email' => $john->email,
            'document' => $john->document,
            'city' => $john->city,
            'state' => $john->state,
            'start_date' => $john->startDate,
            'user_id' => $this->importerUser->id
        ]);

        $this->assertDatabaseHas('employees', [
            'name' => $mary->name,
            'email' => $mary->email,
            'document' => $mary->document,
            'city' => $mary->city,
            'state' => $mary->state,
            'start_date' => $mary->startDate,
            'user_id' => $this->importerUser->id
        ]);
    }

    public function test_it_can_update_an_existing_employee()
    {
        $john = new Employee(
            name: 'João',
            email: new Email('joao@teste.com.br'),
            document: '431.986.320-80',
            city: 'Ribeirão Preto',
            state: 'SP',
            startDate: '2023-05-20',
        );

        $this->repository->import([$john], $this->importerUser);

        $this->assertDatabaseCount('employees', 1);

        $john->name = 'João da Silva (editado)';

        $this->repository->import([$john], $this->importerUser);

        $this->assertDatabaseCount('employees', 1);
        $this->assertDatabaseHas('employees', [
            'name' => 'João da Silva (editado)'
        ]);
    }
}
